import CallHistory from './CallHistory.json';
import React, { Component } from 'react';

var data = CallHistory;
var obj = {};
const action = 'calledCount';

var distinctData = data.map(function (d, idx) {

    var obj = {
        ...data[idx]
    };
    obj[action] = "-1";

  return obj
});
var reducedArray = [];

var orderedDistinctData = distinctData.reduce(function (accumulator, currentValue) {
  if (reducedArray.indexOf(currentValue) === -1) {
    reducedArray.push(currentValue);
  }
  return reducedArray
}, [])
console.log(orderedDistinctData)

class SortedList extends Component {
  render() {

      return (
        <div className="App">
          {data.map(function(d, idx){

             return (

               <div key={idx} className="App-header">

                 {d.firstName + " "}
                 <span className="text-bold">
                    {d.lastName + " "}
                 </span>
                 <span className="text-align-right">
                    {d.called}
                 </span>
             </div>)
           })}
         </div>
      );
    }
  }

export default SortedList;
