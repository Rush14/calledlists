import React, { Component } from 'react';
import './App.css';
import SortedList from './SortedList.js';

class App extends Component {

  render() {

      return (
        <SortedList />
      );
    }
  }

export default App;
